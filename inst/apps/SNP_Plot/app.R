library(shiny)
library(shinydashboard)
library(parallel)
library(dplyr)
library(ggplot2)
library(mmgeSNP)

cl <- makeCluster(detectCores())

fluidigm_data_folder <- "/media/HG-Fingerprints/Fluidigm Data Files"
data_file <- sprintf("data_%s.rda", Sys.Date())

fluidigm_files <- list.files(fluidigm_data_folder, pattern = "*.csv", full.names = TRUE)

if(file.exists(data_file)) {
  load(data_file)
} else {
  data <- do.call(rbind, parLapply(cl, fluidigm_files, function(file) {
    x <- mmgeSNP::read_fluidigm(file)
    x$Run <- sprintf("plate_%s_run_%s", attr(x, "plate"), attr(x, "run"))
    return(x)
  }))
  save(data, file = sprintf("data_%s.rda", Sys.Date()))
}

d <- data[data$Assay_Name == "GTA0048869", ]

r_call <- function(d, clusters, ol_method) {

  if(is.null(clusters)) clusters = 3
  if(is.null(ol_method)) ol_method = "kmeans"

  km <- kmeans(d %>% select(Intensity_X, Intensity_Y), centers = clusters)

  dd <- d
  dd$center_x <- km$centers[km$cluster, 1]
  dd$center_y <- km$centers[km$cluster, 2]
  dd$distance <- sqrt((dd$Intensity_X - dd$center_x)^2 + (dd$Intensity_Y - dd$center_y)^2)

  y <- classInt::classIntervals(dd$distance, n = 2, style = ol_method)

  calls <- c(NA, NA, NA)

  calls[km$centers[,1] - km$centers[,2] == max(km$centers[,1] - km$centers[,2])] <- "XX"
  calls[km$centers[,2] - km$centers[,1] == max(km$centers[,2] - km$centers[,1])] <- "YY"
  calls[is.na(calls)] <- "XY"

  dd <- dd %>%
    mutate(R_Call = calls[km$cluster]) %>%
    mutate(R_Call = ifelse(distance > y$brks[2], "No Call", R_Call)) %>%
    select(-distance, -center_x, -center_y) %>%
    filter(R_Call != "No Call")

  km <- kmeans(dd %>% select(Intensity_X, Intensity_Y), centers = clusters)


  return(d)

}

# Define UI for application that draws a histogram
ui <- dashboardPage(title = "SNP_Viewer",
  dashboardHeader(title = "SNP_Viewer"),
  dashboardSidebar(disable = TRUE),
  dashboardBody(
    column(width = 4,
      box(width = NULL, title = "Plot", status = "warning",
        plotOutput("cluster_plot")
      ),
      box(width = NULL, title = "Options", status = "info",
        selectInput("snp_select", "SNP", choices = sort(unique(data$Experiment.Information_SNP.Assay.and.Allele.Names_Assay))),
        selectInput("run_select", "Runs", choices = sort(unique(data$Run)), multiple = TRUE),
        radioButtons("groupby", "Color By", choices = c("Auto_Call", "Final_Call", "R_Call"), selected = "Final_Call"),
        selectInput("clusters", "Clusters", choices = c(1,2,3), selected = 3),
        selectInput("outlier", "Outlier Method", choices = c("sd", "equal", "pretty", "quantile", "kmeans", "hclust", "bclust", "fisher", "jenks"), selected = "kmeans")
      )
    ),
    column(width = 8,
      box(width = NULL, title = "Data", status = "primary",
        DT::dataTableOutput("snp_data")
      )
    )
  )
)

# Define server logic required to draw a histogram
server <- function(input, output, session) {

  full_data <- reactive({
    data %>%
      select(
        Run,
        Chamber_ID = Experiment.Information_Chamber_ID,
        Assay_Name = Experiment.Information_SNP.Assay.and.Allele.Names_Assay,
        X = Experiment.Information_SNP.Assay.and.Allele.Names_Allele.X,
        Y = Experiment.Information_SNP.Assay.and.Allele.Names_Allele.Y,
        Sample_Name = Experiment.Information_Sample_Name,
        Auto_Call = Results_Call.Information_Auto,
        Auto_Conf = Results_Call.Information_Confidence,
        Final_Call = Results_Call.Information_Final,
        Converted_Call = Results_Call.Information_Converted,
        Intensity_X = Results_Intensity_Allele.X,
        Intensity_Y = Results_Intensity_Allele.Y
      ) %>%
      mutate(Intensity_X = round(Intensity_X, 3)) %>%
      mutate(Intensity_Y = round(Intensity_Y, 3))
  })

  data_subset <- reactive({


    data <- full_data() %>%
      filter(Assay_Name == input$snp_select)

    if(length(input$run_select) > 0) {
      data <- data %>%
        filter(Run %in% input$run_select)
    }

    data <- r_call(data, input$clusters, input$outlier)

    return(data)

  })


  output$snp_data <- DT::renderDataTable({

    data <- data_subset()
    d <<- data
    DT::datatable(data, class = "display compact",
                  extensions = 'Scroller', rownames = FALSE, selection = 'single',
                  options = list(
                    dom = 'lrtip',
                    scrollY = 550,
                    scrollX = TRUE,
                    scroller = TRUE
                  ))
  })

  output$cluster_plot <- renderPlot({

    colors <- c(
      "No Call" = "#999999",
      "YY" = "#e41a1c",
      "XY" = "#984ea3",
      "XX" = "#377eb8"
    )

    data <- data_subset()

    selected_data <<- data[input$snp_data_rows_selected, ]

    m <- max(c(data$Intensity_X, data$Intensity_Y, 1))

    cc <- input$groupby

    p <- ggplot(data, aes_string(x = "Intensity_X", y = "Intensity_Y", color = input$groupby)) +
                  geom_point() + scale_x_continuous(limits = c(0, m)) +
                  scale_y_continuous(limits = c(0, m)) + scale_color_manual(values=colors) + theme_minimal()

    if(nrow(selected_data) > 0) {
      p <- p + geom_point(data = data[input$snp_data_rows_selected, ], aes(x = Intensity_X, y = Intensity_Y), shape = 10, color = "black", size = 4)
    }

    return(p)

  })

}

# Run the application
shinyApp(ui = ui, server = server)

